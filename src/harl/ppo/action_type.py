from enum import Enum, auto


class ActionType(Enum):
    CONTINUOUS = auto()
    DISCRETE = auto()
    OTHER = auto()
