from __future__ import annotations

import io
from typing import List
from unittest import TestCase

import torch as T
import numpy as np

from palaestrai.agent.util.information_utils import (
    concat_flattened_act_scale_bias,
)
from palaestrai.types import Box
from palaestrai.agent import SensorInformation, ActuatorInformation

from harl.sac.muscle import SACMuscle
from harl.sac.network import Actor, ActionType


class SACMuscleTestCase(TestCase):
    @staticmethod
    def create_actor(
        sensors: List[SensorInformation], actuators: List[ActuatorInformation]
    ):
        assert isinstance(sensors, List)
        assert isinstance(actuators, List)

        return Actor(
            obs_dim=int(
                np.sum(
                    [
                        np.prod(s.space.shape)  # type: ignore[attr-defined]
                        for s in sensors
                    ]
                )
            ),
            act_dim=int(
                np.sum(
                    [
                        np.prod(a.space.shape)  # type: ignore[attr-defined]
                        for a in actuators
                    ]
                )
            ),
            act_scale=T.tensor(
                concat_flattened_act_scale_bias(actuators, np.subtract),
                dtype=T.float32,
            ),
            act_bias=T.tensor(
                concat_flattened_act_scale_bias(actuators, np.add),
                dtype=T.float32,
            ),
            action_type=ActionType.CONTINUOUS,
            fc_dims=[8, 8],
        )

    def setUp(self) -> None:
        self.sensors = [
            SensorInformation(
                np.array([0], dtype=np.float32),
                Box([0], [1], shape=(1,), dtype=np.float32),
                "s-1",
            ),
            SensorInformation(
                np.array([1], dtype=np.float32),
                Box([1], [2], shape=(1,), dtype=np.float32),
                "s-2",
            ),
        ]
        self.actuators = [
            ActuatorInformation(
                np.array([0], dtype=np.float32),
                Box([0], [1], shape=(1,), dtype=np.float32),
                "a-1",
            ),
            ActuatorInformation(
                np.array([1], dtype=np.float32),
                Box([1], [2], shape=(1,), dtype=np.float32),
                "a-2",
            ),
        ]
        self.muscle = SACMuscle()
        self.muscle._uid = "sac_muscle_uid"
        self.muscle.setup()

    def test_propose_actions(self):
        actor = SACMuscleTestCase.create_actor(self.sensors, self.actuators)
        bio = io.BytesIO()
        T.save(actor, bio)
        bio.seek(0)
        self.muscle.update(bio)
        act = self.muscle.propose_actions(self.sensors, self.actuators)
        self.assertTrue(all([a.value is not None for a in act[0]]))
        self.assertEqual(len(act[0]), len(self.actuators))

    def test_propose_actions_multidimensional_actuators(self):
        actuators = [
            ActuatorInformation(
                np.array([2.0, 3.0, 4.0], dtype=np.float32),
                Box(
                    [2.0, 3.0, 4.0],
                    [4.0, 5.0, 6.0],
                    shape=(3,),
                    dtype=np.float32,
                ),
                "a-2",
            ),
        ]
        actor = SACMuscleTestCase.create_actor(self.sensors, actuators)
        self.muscle._model = actor

        actions = self.muscle.propose_actions(self.sensors, actuators)
        self.assertEqual(len(actions[0]), len(actuators))
        self.assertIsNotNone(actions[0][0].value)
