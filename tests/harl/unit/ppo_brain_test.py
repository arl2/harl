from __future__ import annotations

from unittest import TestCase

import numpy
import numpy as np
import torch.nn

from palaestrai.agent import SensorInformation, ActuatorInformation
from palaestrai.types import Box

from harl.ppo.brain import PPOBrain

fc_dims = (8, 8)


class PPOBrainTest(TestCase):
    def setUp(self) -> None:
        self.sensors = [
            SensorInformation(
                np.array([0, 0], dtype=np.float32),
                Box([-2, -2], [3, 3], shape=(2,), dtype=np.float32),
                "Tile 1-1",
            )
        ]
        self.actuators = [
            ActuatorInformation(
                np.array([0], dtype=np.float32),
                Box([-2], [3], shape=(1,), dtype=np.float32),
                "Field " "selector",
            ),
            ActuatorInformation(
                np.array([0], dtype=np.float32),
                Box([-2], [3], shape=(1,), dtype=np.float32),
                "Field " "selector",
            ),
        ]
        self.ppo_brain = PPOBrain()
        self.ppo_brain._seed = 42
        self.ppo_brain._sensors = self.sensors
        self.ppo_brain._actuators = self.actuators
        self.ppo_brain.setup()

    def test_network(self):
        """Tests if the network is set up properly"""
        actor = self.ppo_brain.actor.actor
        critic = self.ppo_brain.critic.critic

        actor_layer_num = (
            1 + len(fc_dims) - 1 + 1
        ) * 2  # In, Hidden, Out and a function for each
        critic_layer_num = actor_layer_num - 1  # Critic out has no func, so -1

        # Tests correct amount of layers
        self.assertEqual(len(actor), actor_layer_num)
        self.assertEqual(len(critic), critic_layer_num)

        # Tests correct connection of layers
        out_features = len(self.sensors)
        for layer in actor:
            if not hasattr(layer, "in_features"):
                continue  # Functions
            self.assertEqual(out_features, layer.in_features)
            out_features = layer.out_features
        self.assertEqual(len(self.actuators), out_features)  # Output layer

        out_features = len(self.sensors)
        for layer in critic:
            if not hasattr(layer, "in_features"):
                continue  # Functions
            self.assertEqual(layer.in_features, out_features)
            out_features = layer.out_features
        self.assertEqual(1, out_features)  # Output layer

    def test_network_init(self):
        """Tests if the network initialises the weight and bias properly"""
        actor = self.ppo_brain.actor.actor
        critic = self.ppo_brain.critic.critic

        i = 0
        for layer in actor:
            i += 1
            if isinstance(layer, torch.nn.Linear):
                weights = layer.weight.detach().cpu().numpy()
                bias = layer.bias.detach().cpu().numpy()
                self.assertLess(0, numpy.count_nonzero(weights))
                if i > 1:
                    # torch's zeros tensor are filled with 0.x values; casting to int to get rid of the decimal
                    for num in bias:
                        self.assertEqual(0, int(num))

        i = 0
        for layer in critic:
            i += 1
            if isinstance(layer, torch.nn.Linear):
                weights = layer.weight.detach().cpu().numpy()
                bias = layer.bias.detach().cpu().numpy()
                self.assertLess(0, numpy.count_nonzero(weights))
                if i > 1:
                    for num in bias:
                        self.assertEqual(0, int(num))

    def test_thinking(self):
        self.ppo_brain.thinking("MuscleID", "Values")

    def test_action_std_decay(self):
        """Tests if the decay of the std works"""
        for i in range(0, 1000):
            std_before = self.ppo_brain.action_std
            self.ppo_brain.decay_action_std()  # Make decay step
            std_after = self.ppo_brain.action_std
            self.assertGreaterEqual(
                std_before, std_after
            )  # Old std should always be bigger or equal to the new one
            if (
                std_before == std_after
            ):  # Old and new should only be equal when reaching the minimum
                self.assertEqual(std_after, self.ppo_brain.min_action_std)
