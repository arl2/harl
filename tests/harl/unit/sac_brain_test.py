from __future__ import annotations

from unittest import TestCase
from unittest.mock import patch

import numpy as np
import torch

from harl.sac.brain import SACBrain
from harl.sac.network import Actor, Critic
from palaestrai.agent import SensorInformation, ActuatorInformation
from palaestrai.types import Box


class SACBrainTest(TestCase):
    def setUp(self) -> None:
        self.sensors = [
            SensorInformation(
                np.array([0, 0], dtype=np.float32),
                Box([-2, -2], [3, 3], shape=(2,), dtype=np.float32),
                "Tile 1-1",
            )
        ]
        self.actuators = [
            ActuatorInformation(
                np.array([0], dtype=np.float32),
                Box([-2], [3], shape=(1,), dtype=np.float32),
                "Field " "selector",
            ),
            ActuatorInformation(
                np.array([0], dtype=np.float32),
                Box([-2], [3], shape=(1,), dtype=np.float32),
                "Field " "selector",
            ),
        ]
        self.sac_brain = SACBrain()
        self.sac_brain._seed = 42
        self.sac_brain._sensors = self.sensors
        self.sac_brain._actuators = self.actuators
        self.sac_brain.setup()

    def test_alpha_annealing(self):
        for i in range(5):
            current_alpha = self.sac_brain._alpha
            o = torch.Tensor([1, 1])
            pi, logp_pi, _ = self.sac_brain.actor.pi(o)
            self.sac_brain._anneal_alpha(logp_pi)
            self.assertNotEqual(current_alpha, self.sac_brain._alpha)

    def test_pre_load_models(self):
        pre_load_sac_brain = SACBrain()
        pre_load_sac_brain._seed = 42
        pre_load_sac_brain._sensors = self.sensors
        pre_load_sac_brain._actuators = self.actuators
        with patch(
            "harl.sac.brain.SACBrain._init_models",
            wraps=pre_load_sac_brain._init_models,
        ) as mock_init_models:
            pre_load_sac_brain.setup()

        mock_init_models.assert_called_once()

        assert type(pre_load_sac_brain.actor) is Actor
        assert type(pre_load_sac_brain.actor_target) is Actor
        assert type(pre_load_sac_brain.critic) is Critic
        assert type(pre_load_sac_brain.critic_target) is Critic

        with patch(
            "harl.sac.brain.SACBrain._init_models",
            wraps=pre_load_sac_brain._init_models,
        ) as mock_init_models:
            pre_load_sac_brain.setup()

        mock_init_models.assert_not_called()

    def test_pretrain(self):
        self.sac_brain.memory.append(
            "pretrainer_muscle",
            sensor_readings=[
                SensorInformation(
                    np.array([0, 0], dtype=np.float32),
                    Box([-2, -2], [3, 3], shape=(2,), dtype=np.float32),
                    "Tile 1-1",
                )
            ],
            actuator_setpoints=[],
            rewards=[],
            objective=1.0,
        )
