from __future__ import annotations

import copy
from unittest import TestCase

import numpy as np
import torch
import torch as T
from palaestrai.agent import SensorInformation, ActuatorInformation
from palaestrai.types import Box

from harl.ppo.muscle import PPOMuscle
from harl.ppo.network import ActorNetwork, CriticNetwork
from harl.ppo.action_type import ActionType


class PPOMuscleTestCase(TestCase):
    @staticmethod
    def create_actor(
        sensors: list[SensorInformation], actuators: list[ActuatorInformation]
    ):
        assert isinstance(sensors, list)
        assert isinstance(actuators, list)

        return {
            "actor": ActorNetwork(
                state_dim=len(sensors),
                action_dim=len(actuators),
                action_type=ActionType.CONTINUOUS,
            ),
            "critic": CriticNetwork(state_dim=len(sensors)),
        }

    def setUp(self) -> None:
        self.sensors = [
            SensorInformation(
                0.0,
                Box(0, 1, shape=()),
                "s-1",
            ),
            SensorInformation(
                1.0,
                Box(1, 2, shape=()),
                "s-2",
            ),
        ]
        self.actuators = [
            ActuatorInformation(
                0.0,
                Box(0, 1, shape=()),
                "a-1",
            ),
            ActuatorInformation(
                1.0,
                Box(1, 2, shape=()),
                "a-2",
            ),
        ]
        self.muscle = PPOMuscle()
        self.muscle._uid = "ppo_muscle_uid"
        self.muscle.setup()

        self.muscle.update(
            PPOMuscleTestCase.create_actor(self.sensors, self.actuators)
        )

    def test_propose_actions(self):
        """Tests if a proposed action is viable"""
        act = self.muscle.propose_actions(self.sensors, self.actuators)

        self.assertTrue(all([a.value is not None for a in act[0]]))
        self.assertEqual(len(act[0]), len(self.actuators))

    def test_update(self):
        """Tests if the networks state gets updated"""
        assert self.muscle.actor is not None
        assert self.muscle.critic is not None
        actor_dict_old = copy.deepcopy(self.muscle.actor.state_dict())
        critic_dict_old = copy.deepcopy(self.muscle.critic.state_dict())

        net_dict = PPOMuscleTestCase.create_actor(self.sensors, self.actuators)
        net_dict["actor"].load_state_dict(
            {
                "actor.0.weight": T.ones(256, len(self.sensors)),
                "actor.0.bias": T.ones(256),
                "actor.2.weight": T.ones(256, 256),
                "actor.2.bias": T.ones(256),
                "actor.4.weight": T.ones(len(self.actuators), 256),
                "actor.4.bias": T.ones(len(self.actuators)),
            }
        )
        net_dict["critic"].load_state_dict(
            {
                "critic.0.weight": T.ones(256, len(self.sensors)),
                "critic.0.bias": T.ones(256),
                "critic.2.weight": T.ones(256, 256),
                "critic.2.bias": T.ones(256),
                "critic.4.weight": T.ones(1, 256),
                "critic.4.bias": T.ones(1),
            }
        )
        self.muscle.update(net_dict)

        actor_dict_new = self.muscle.actor.state_dict()
        critic_dict_new = self.muscle.critic.state_dict()

        for i in range(0, 5, 2):
            self.assertFalse(
                torch.equal(
                    actor_dict_old[f"actor.{i}.weight"],
                    actor_dict_new[f"actor.{i}.weight"],
                )
            )
            self.assertFalse(
                torch.equal(
                    actor_dict_old[f"actor.{i}.bias"],
                    actor_dict_new[f"actor.{i}.bias"],
                )
            )

        for i in range(0, 5, 2):
            self.assertFalse(
                torch.equal(
                    critic_dict_old[f"critic.{i}.weight"],
                    critic_dict_new[f"critic.{i}.weight"],
                )
            )
            self.assertFalse(
                torch.equal(
                    critic_dict_old[f"critic.{i}.bias"],
                    critic_dict_new[f"critic.{i}.bias"],
                )
            )
